# alpine-ssh-rsync

Alpine + ssh + rsync

## Environnement

- `SSH_PRIVATE_KEY` : Injects private key, if none provided one will be generated
- `SSH_KNOWN_HOSTS` : Injects known hosts, hosts will not be checked if none provided
