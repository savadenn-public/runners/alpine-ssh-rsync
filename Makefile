build:
	docker build -t savadenn/alpine-ssh-rsync .

test: build
	@docker run --rm -it savadenn/alpine-ssh-rsync sh
