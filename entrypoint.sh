#!/usr/bin/env sh
echo "Initiating SSH"

eval "$(ssh-agent -s)"

if [ -z "${SSH_PRIVATE_KEY+x}" ]; then
  echo "[INFO] No private key provided, generating one"
  ssh-keygen -t rsa -b 4096 -o -a 100 -N "" -f ~/.ssh/id_rsa
else
  echo "${SSH_PRIVATE_KEY}" | tr -d '\r' | ssh-add -
fi

if [ -z "${SSH_KNOWN_HOSTS+x}" ]; then
  echo "[WARN] No known host provided, disabling host verification"
  echo "Host *
   StrictHostKeyChecking no
   UserKnownHostsFile=/dev/null" > ~/.ssh/config
else
  echo "${SSH_KNOWN_HOSTS}" > ~/.ssh/known_hosts
  chmod 644 ~/.ssh/known_hosts
fi

exec "$@"
