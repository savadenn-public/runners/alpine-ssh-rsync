FROM alpine:latest

RUN apk add --no-cache rsync openssh-client rsync sed
RUN mkdir -p ~/.ssh && chmod 700 ~/.ssh

COPY entrypoint.sh /

ENTRYPOINT ["/entrypoint.sh"]
